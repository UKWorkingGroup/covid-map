
//  .. OSM Toner
var toner = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
			{attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
			 "opacity": 0.2, "minZoom": 4, "maxZoom": 13,"noWrap": false,"maxNativeZoom": 13});


// Overlay layers (TMS)
var covid_url='./';
var covid_url='https://gitlab.com/weirdos-and-misfits/covid-map/-/raw/master/';
var covid_deaths = L.tileLayer(covid_url+'data/covid_total/{z}/{x}/{y}.png',
		      {tms: true,opacity: 1.0, attribution: "", minZoom: 4, maxZoom: 13});
		      //"maxNativeZoom": 6,"noWrap": false});

//10
var map = L.map('map').setView([53.3951, -1.477], 7).addLayer(covid_deaths).addLayer(toner);
map.zoomControl.setPosition('bottomright');

var legend = L.control({position: 'topleft'});

legend.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info legend');

    this._div = L.DomUtil.create('div', 'info legend');
    this.update();
    return this._div;
};
legend.update = function(props) {
    this._div.innerHTML = '<img src="./images/legend.png" top="100" width="70%" alt="legend">';
};
legend.addTo(map);




L.Control.geocoder().addTo(map);

var overlaymaps = {"Stamen Toner": toner}//, "CDRC": toner2}
var basemaps = {"Total COVID-19 Deaths x": covid_deaths}

// Title
var title = L.control();
title.onAdd = function(map) {
    this._div = L.DomUtil.create('div', 'ctl title');
    this.update();
    return this._div;
};
title.update = function(props) {
    this._div.innerHTML = "The Working Group";
};
title.addTo(map);


// Add base layers
L.control.layers(basemaps, overlaymaps).addTo(map);


